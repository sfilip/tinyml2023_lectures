{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TP2 - The PyTorch Workflow and Applications To Image Classification "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Acknowledgements**: inspired by the following PyTorch tutorials: [here](https://www.learnpytorch.io/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading the required libraries\n",
    "\n",
    "Before we get started writing code, let us discuss about some PyTorch computer vision libraries you should be aware of. You have already seen them used in the second demo from last week's lecture.\n",
    "\n",
    "| PyTorch module | What does it do? |\n",
    "| ----- | ----- |\n",
    "| [`torchvision`](https://pytorch.org/vision/stable/index.html) | Contains datasets, model architectures and image transformations often used for computer vision problems. |\n",
    "| [`torchvision.datasets`](https://pytorch.org/vision/stable/datasets.html) | Here you'll find many example computer vision datasets for a range of problems from image classification, object detection, image captioning, video classification and more. It also contains [a series of base classes for making custom datasets](https://pytorch.org/vision/stable/datasets.html#base-classes-for-custom-datasets). |\n",
    "| [`torchvision.models`](https://pytorch.org/vision/stable/models.html) | This module contains well-performing and commonly used computer vision model architectures implemented in PyTorch, you can use these with your own problems. | \n",
    "| [`torchvision.transforms`](https://pytorch.org/vision/stable/transforms.html) | Often images need to be transformed (turned into numbers/processed/augmented) before being used with a model, common image transformations are found here. | \n",
    "| [`torch.utils.data.Dataset`](https://pytorch.org/docs/stable/data.html#torch.utils.data.Dataset) | Base dataset class for PyTorch.  | \n",
    "| [`torch.utils.data.DataLoader`](https://pytorch.org/docs/stable/data.html#module-torch.utils.data) | Creates a Python iterable over a dataset (created with `torch.utils.data.Dataset`). |\n",
    "\n",
    "> **Note:** The `torch.utils.data.Dataset` and `torch.utils.data.DataLoader` classes aren't only for computer vision in PyTorch, they are capable of dealing with many different types of data.\n",
    "\n",
    "Now we've covered some of the most important PyTorch computer vision libraries, let's import the relevant dependencies.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import torch\n",
    "import torch\n",
    "from torch import nn\n",
    "\n",
    "# import torchvision\n",
    "import torchvision\n",
    "from torchvision import datasets\n",
    "from torchvision import transforms\n",
    "\n",
    "# import matplotlib for visualization\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting a dataset\n",
    "\n",
    "Whereas we already saw MNIST, we are now going to use [FashionMNIST](https://github.com/zalandoresearch/fashion-mnist), made by Zalando Research, which has a similar setup. The difference is that it contains grayscale images of 10 different kinds of clothing.\n",
    "\n",
    "> **Note:** `torchvision.datasets` contains many different datasets you can use to practice writing computer vision DNNs on. MNIST and FashionMNIST are just two examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Downloading FashionMNIST is done through the [`torchvision.datasets.FashionMNIST()`](https://pytorch.org/vision/main/generated/torchvision.datasets.FashionMNIST.html) function. To use it, we need to provide the following parameters:\n",
    "* `root: str` - which folder do you want to download the data to?\n",
    "* `train: Bool` - do you want the training or test split?\n",
    "* `download: Bool` - should the data be downloaded?\n",
    "* `transform: torchvision.transforms` - what transformations would you like to do on the data?\n",
    "* `target_transform` - you can transform the targets (labels) if you like too.\n",
    "\n",
    "Many other datasets in `torchvision` have these parameter options."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setup training data\n",
    "train_data = datasets.FashionMNIST(\n",
    "    root=\"data\", # where to download data to? ('data' sub-directory of the current directory)\n",
    "    train=True, # get training data\n",
    "    download=True, # download data if it doesn't exist on disk\n",
    "    transform=transforms.Compose(\n",
    "      [transforms.ToTensor(), \n",
    "       transforms.Normalize(mean=(0.1307,), std=(0.3081,))]), # images come as PIL format, we want to turn into Torch tensors and normalize them\n",
    "    target_transform=None # you can transform labels as well\n",
    ")\n",
    "\n",
    "# Setup testing data\n",
    "test_data = datasets.FashionMNIST(\n",
    "    root=\"data\",\n",
    "    train=False, # get test data\n",
    "    download=True,\n",
    "    transform=transforms.Compose(\n",
    "      [transforms.ToTensor(), \n",
    "       transforms.Normalize(mean=(0.1307,), std=(0.3081,))]), # images come as PIL format, we want to turn into Torch tensors and normalize them\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Checking out the shape of our data, we see that it is a 1-channel image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# See first training sample\n",
    "image, label = train_data[0]\n",
    "# What's the shape of the image?\n",
    "print(image.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The shape of the image tensor is `[1, 28, 28]` or more specifically:\n",
    "\n",
    "```\n",
    "[color_channels=1, height=28, width=28]\n",
    "```\n",
    "\n",
    "Having `color_channels=1` means the image is grayscale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# How many samples are there? \n",
    "len(train_data.data), len(train_data.targets), len(test_data.data), len(test_data.targets)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we've got 60,000 training samples and 10,000 testing samples.\n",
    "\n",
    "What classes are there?\n",
    "\n",
    "We can find these via the `.classes` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# See classes\n",
    "class_names = train_data.classes\n",
    "class_names"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also examine elements in the training set to see how they look like. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot more images\n",
    "torch.manual_seed(42)\n",
    "fig = plt.figure(figsize=(9, 9))\n",
    "rows, cols = 4, 4\n",
    "for i in range(1, rows * cols + 1):\n",
    "    random_idx = torch.randint(0, len(train_data), size=[1]).item()\n",
    "    img, label = train_data[random_idx]\n",
    "    fig.add_subplot(rows, cols, i)\n",
    "    plt.imshow(img.squeeze(), cmap=\"gray\")\n",
    "    plt.title(class_names[label])\n",
    "    plt.axis(False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To process the data in a training scenario, we need to prepare a data loader. This is done using[`torch.utils.data.DataLoader`](https://pytorch.org/docs/stable/data.html#torch.utils.data.Dataset) or `DataLoader` for short. It turns a large `Dataset` into a Python iterable of smaller chunks.\n",
    "\n",
    "These smaller chunks are called **batches** or **mini-batches** and can be set by the `batch_size` parameter.\n",
    "We do this because it is more computationally efficient.\n",
    "\n",
    "In an ideal world you could do the forward pass and backward pass across all of your data at once. But once you start using really large datasets, unless you have infinite computing power (and let's face it, who does?), it's easier to break them up into batches. It also gives your model more opportunities to improve.\n",
    "\n",
    "With **mini-batches** (small portions of the data), gradient descent is performed more often per epoch (once per mini-batch rather than once per epoch).\n",
    "\n",
    "What is a good batch size?\n",
    "\n",
    "[32 is a good place to start](https://twitter.com/ylecun/status/989610208497360896?s=20&t=N96J_jotN--PYuJk2WcjMw) for a fair amount of problems.\n",
    "\n",
    "But since this is a value you can set (a **hyperparameter**) you can try all different kinds of values, though generally powers of 2 are used most often (e.g. 32, 64, 128, 256, 512).\n",
    "\n",
    "Let us create `DataLoader`'s for our training and test sets. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from torch.utils.data import DataLoader\n",
    "\n",
    "# Setup the batch size hyperparameter\n",
    "batch_size = 32\n",
    "\n",
    "# Turn datasets into iterables (batches)\n",
    "train_dataloader = DataLoader(train_data, # dataset to turn into iterable\n",
    "    batch_size=batch_size, # how many samples per batch? \n",
    "    shuffle=True # shuffle data every epoch?\n",
    ")\n",
    "\n",
    "test_dataloader = DataLoader(test_data,\n",
    "    batch_size=batch_size,\n",
    "    shuffle=False # don't necessarily have to shuffle the testing data\n",
    ")\n",
    "\n",
    "# Let's check out what we've created\n",
    "print(f\"Dataloaders: {train_dataloader, test_dataloader}\") \n",
    "print(f\"Length of train dataloader: {len(train_dataloader)} batches of {batch_size}\")\n",
    "print(f\"Length of test dataloader: {len(test_dataloader)} batches of {batch_size}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic architecture of a neural network for classification\n",
    "\n",
    "Before we get into writing code, let us look at the general architecture of a classification neural network.\n",
    "\n",
    "| **Hyperparameter** | **Classification** |\n",
    "| --- | --- |\n",
    "| **Input layer shape** (`in_features`) | Same as number of features (e.g. 5 for age, sex, height, weight, smoking status in heart disease prediction) |\n",
    "| **Hidden layer(s)** | Problem specific, minimum = 1, maximum = unlimited | \n",
    "| **Neurons per hidden layer** | Problem specific, generally 10 to 512 | \n",
    "| **Output layer shape** (`out_features`) | 1 per class (e.g. 3 for food, person or dog photo) |\n",
    "| **Hidden layer activation** | Usually [ReLU](https://pytorch.org/docs/stable/generated/torch.nn.ReLU.html#torch.nn.ReLU) (rectified linear unit) but [can be many others](https://en.wikipedia.org/wiki/Activation_function#Table_of_activation_functions) |\n",
    "| **Output activation** | [Softmax](https://en.wikipedia.org/wiki/Softmax_function) ([`torch.softmax`](https://pytorch.org/docs/stable/generated/torch.nn.Softmax.html) in PyTorch) |\n",
    "| **Loss function** | Cross entropy ([`torch.nn.CrossEntropyLoss`](https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html) in PyTorch) |\n",
    "| **Optimizer** | [SGD](https://pytorch.org/docs/stable/generated/torch.optim.SGD.html) (stochastic gradient descent), [Adam](https://pytorch.org/docs/stable/generated/torch.optim.Adam.html) (see [`torch.optim`](https://pytorch.org/docs/stable/optim.html) for more options) |\n",
    "\n",
    "Of course, this ingredient list of classification neural network components will vary depending on the problem you are working on. But it is more than enough to get started. We are going to get hands-on with this setup throughout this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multilayer Perceptron Networks\n",
    "\n",
    "We are going to use the same networks as in the MNIST demo shown during class. Let us first start with the three layer MLP."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Utility function to see if execution can be done on a CUDA-enabled GPU; if none present on your system it will default to CPU\n",
    "def try_gpu():\n",
    "    return \"cuda\" if torch.cuda.is_available() else \"cpu\"\n",
    "\n",
    "\n",
    "# 1. Construct a model class that subclasses nn.Module\n",
    "class MLP(nn.Module):\n",
    "  def __init__(self):\n",
    "    super(MLP, self).__init__()\n",
    "    # 2. Create the three nn. Linear layers that make up our MLP model and the activation functions after each layer (the last layer activation (i.e. the Softmax) is not needed, since it is applied together with the CrossEntropy loss function later on)\n",
    "    self.l1 = nn.Linear(784, 128) # takes in a 1D-version of the 28x28 input image (size 28*28=784),\n",
    "                                  # produces 128 output features\n",
    "    self.relu1 = nn.ReLU()        # the activation function to be applied after the the first layer\n",
    "    self.l2 = nn.Linear(128, 96)  # second linear layer; takes 128 input features (the output of\n",
    "                                  # previous layer) and outputs 96 features\n",
    "    self.relu2 = nn.ReLU()        # the activation applied after the second layer\n",
    "    self.l3 = nn.Linear(96, 10)   # third linear layer; takes 96 input features (the outpu of the\n",
    "                                  # second layer) and outputs 10 features (the number of classes)\n",
    "\n",
    "  # 3. Define a forward method containing the forward pass computation\n",
    "  def forward(self, x):\n",
    "    # first reshape the input image(s) into 1D tensors that are understood by linear layers\n",
    "    x = x.reshape(-1, 28 * 28)\n",
    "    # compute the output of the first layer and the first activation\n",
    "    out = self.relu1(self.l1(x))\n",
    "    # compute the output of the second layer and the second activation\n",
    "    out = self.relu2(self.l2(out))\n",
    "    # compute the output of the last (third) layer\n",
    "    out = self.l3(out)\n",
    "    # return the result of applying all the layers in succession\n",
    "    return out\n",
    "  \n",
    "# 4. Create an instance of the model and send it to the target device \n",
    "# (if you have a GPU available and enabled in PyTorch)\n",
    "torch.manual_seed(42)\n",
    "model = MLP().to(device=try_gpu())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us also set up the hyperparameters for our training task: the learning rate for the optimizer, the number of epochs, the cost function and the optimizer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from torch import optim\n",
    "# Define relevant variable for the ML task\n",
    "learning_rate = 0.01\n",
    "num_epochs = 10\n",
    "\n",
    "# Setting the loss function\n",
    "cost = nn.CrossEntropyLoss()\n",
    "\n",
    "# Setting the optimizer with the model parameters and learning rate\n",
    "optimizer = optim.SGD(model.parameters(), lr=learning_rate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Training our model means iterating over the mini batches of the training set and, for each mini-batch, computing the forward and backward passes that are needed to provide the optimizer with the gradient it requires to update the model paremeters. Below is a prototypical function for a training function (it is the same one as in the in-class demo)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tqdm import tqdm\n",
    "\n",
    "# Utility function to evaluate the accuracy\n",
    "def get_performance_metrics(net, iter, loss_fn, device=try_gpu()):\n",
    "    # We enter evaluation mode. This is useless for the linear model\n",
    "    # but is important with layers such as dropout, batchnorm, ..\n",
    "    net.eval()\n",
    "    tot_loss, num_correct, num_total = 0.0, 0.0, 0.0\n",
    "    # We disable gradient computation which speeds up the overall computation \n",
    "    # and reduces memory usage \n",
    "    with torch.no_grad():\n",
    "        for i, (X, y) in enumerate(iter):\n",
    "            # We got a minibatch from the loader within inputs and targets\n",
    "            # We need to copy the data on the GPU if we use one\n",
    "            X, y = X.to(device), y.to(device)\n",
    "            # Compute the forward pass, i.e., the scores for each input image\n",
    "            yhat = net(X)\n",
    "\n",
    "            # We accumulate the loss considering\n",
    "            # The multipliation by inputs.shape[0] is due to the fact\n",
    "            # that our loss criterion is averaging over its samples\n",
    "            tot_loss += X.shape[0] * loss_fn(yhat, y).item()\n",
    "\n",
    "            # For the accuracy, we compute the labels for each input image\n",
    "            # Be carefull, the model is outputing scores and not the probabilities\n",
    "            # But given the softmax is not altering the rank of its input scores\n",
    "            # we can compute the label by argmaxing directly the scores\n",
    "            predicted = yhat.argmax(dim=1)\n",
    "            correct = (predicted == y).sum()\n",
    "            num_correct += correct.item()\n",
    "            # We accumulate the exact number of processed samples\n",
    "            num_total += y.shape[0]\n",
    "    return tot_loss / num_total, num_correct / num_total\n",
    "\n",
    "\n",
    "def get_lr(optimizer):\n",
    "    for param_group in optimizer.param_groups:\n",
    "        return param_group[\"lr\"]\n",
    "\n",
    "def trainer(\n",
    "    net,\n",
    "    train_loader,\n",
    "    test_loader,\n",
    "    num_epochs,\n",
    "    lr,\n",
    "    batch_size,\n",
    "    loss,\n",
    "    optimizer,\n",
    "    device=try_gpu()\n",
    "):\n",
    "    # 1. Move the model to the appropriate device for training\n",
    "    net.to(device)\n",
    "    train_acc_list = []\n",
    "    test_acc_list = []\n",
    "\n",
    "    # 2. the training loop\n",
    "    for epoch in range(num_epochs):\n",
    "        # use a tqdm progress bar to see how training progresses\n",
    "        tq = tqdm(total=len(train_loader) * batch_size)\n",
    "        tq.set_description(f\"Epoch {epoch}, lr {get_lr(optimizer):.3f}\")\n",
    "        net.train()\n",
    "        train_acc, train_loss, train_size = 0.0, 0.0, 0.0\n",
    "        for X, y in train_loader:\n",
    "            X, y = X.to(device), y.to(device)\n",
    "            optimizer.zero_grad()\n",
    "            # compute the output\n",
    "            yhat = net(X)\n",
    "            # compute loss and perform back-propagation\n",
    "            l = loss(yhat, y)\n",
    "\n",
    "            # scale gradient and record loss\n",
    "            l.backward()\n",
    "            optimizer.step()\n",
    "\n",
    "            tq.update(batch_size)\n",
    "            with torch.no_grad():\n",
    "                train_loss += l.data.item() * X.shape[0]\n",
    "                pred = yhat.argmax(dim=1)\n",
    "            train_acc += (pred == y).sum().item()\n",
    "            train_size += X.shape[0]\n",
    "\n",
    "            tq.set_postfix(\n",
    "                train_acc=\"{:.5f}\".format(train_acc / train_size),\n",
    "                train_loss=\"{:.5f}\".format(train_loss / train_size),\n",
    "            )\n",
    "        tq.close()\n",
    "\n",
    "        # evaluate the accuracy on the test set at the end of an epoch and add both\n",
    "        # the train and test accuracies to corresponding lists\n",
    "        _, test_acc = get_performance_metrics(net, test_loader, loss, device)\n",
    "        train_acc_list.append(train_acc / train_size)\n",
    "        test_acc_list.append(test_acc)\n",
    "\n",
    "        print(\n",
    "            f\"train loss estimate {train_loss / train_size:.3f}, train acc {train_acc / train_size:.3f}, test acc {test_acc:.3f}\"\n",
    "        )\n",
    "\n",
    "    return train_acc_list, test_acc_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to start training our network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save the train and test accuracies at the end of each epoch in two output lists from the trainer function\n",
    "train_accs_mlp, test_accs_mlp = trainer(net=model, train_loader=train_dataloader, \n",
    "                                test_loader=test_dataloader, num_epochs=num_epochs, lr=learning_rate, batch_size=batch_size, loss=cost, optimizer=optimizer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the outputs of the `trainer` function to examine how the accuracy on the train and test sets evolves as we progress through training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(range(num_epochs), [acc * 100 for acc in train_accs_mlp], label=\"Training Accuracy\")\n",
    "plt.plot(range(num_epochs), [acc * 100 for acc in test_accs_mlp], label=\"Test Accuracy\")\n",
    "plt.xlabel(\"Epoch\")\n",
    "plt.ylabel(\"Accuracy\")\n",
    "plt.legend()\n",
    "plt.title(\"MLP\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "\n",
    "1. Create a new MLP model similar to the previous one, but that now has [BatchNorm1d](https://pytorch.org/docs/stable/generated/torch.nn.BatchNorm1d.html) layers between `l1` and `relu1`, and `l2` and `relu2`; call them `bn1` and `bn2`. Train the new model for 10 epochs using the same hyperparameters as before. How does the test accuracy with BatchNorm compare to the one of the previous network without? Plot the evolution of the test accuracies in both networks on the same graph to compare.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: write your answer here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Saving and loading models\n",
    "\n",
    "Once you have trained a model, chances are that you will want to export it somewhere. PyTorch provides three main methods for saving and loading models that you should be aware of (all of them have been taken from the [PyTorch saving and loading models guide](https://pytorch.org/tutorials/beginner/saving_loading_models.html#saving-loading-model-for-inference)):\n",
    "\n",
    "| PyTorch method | What does it do? | \n",
    "| ----- | ----- |\n",
    "| [`torch.save`](https://pytorch.org/docs/stable/torch.html?highlight=save#torch.save) | Saves a serialized object to disk using Python's [`pickle`](https://docs.python.org/3/library/pickle.html) utility. Models, tensors and various other Python objects like dictionaries can be saved using `torch.save`.  | \n",
    "| [`torch.load`](https://pytorch.org/docs/stable/torch.html?highlight=torch%20load#torch.load) | Uses `pickle`'s unpickling features to deserialize and load pickled Python object files (like models, tensors or dictionaries) into memory. You can also set which device to load the object to (CPU, GPU etc). |\n",
    "| [`torch.nn.Module.load_state_dict`](https://pytorch.org/docs/stable/generated/torch.nn.Module.html?highlight=load_state_dict#torch.nn.Module.load_state_dict)| Loads a model's parameter dictionary (`model.state_dict()`) using a saved `state_dict()` object. | \n",
    "\n",
    "> **Note:** As stated in [Python's `pickle` documentation](https://docs.python.org/3/library/pickle.html), the `pickle` module **is not secure**. That means you should only ever unpickle (load) data you trust. That goes for loading PyTorch models as well. Only ever use saved PyTorch models from sources you trust."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What is a model `state_dict()`?\n",
    "\n",
    "The [recommended way](https://pytorch.org/tutorials/beginner/saving_loading_models.html#saving-loading-model-for-inference) for saving and loading a model for inference (making predictions) is by saving and loading a model's `state_dict()`.\n",
    "\n",
    "In PyTorch, the learnable parameters (i.e. weights and biases) of an `torch.nn.Module` model are contained in the model’s parameters (accessed with `model.parameters()`). A *state_dict* is simply a Python dictionary object that maps each layer to its parameter tensor. Note that only layers with learnable parameters (convolutional layers, linear layers, etc.) and registered buffers (batchnorm’s running_mean) have entries in the model’s *state_dict*. Optimizer objects (`torch.optim`) also have a *state_dict*, which contains information about the optimizer’s state, as well as the hyperparameters used.\n",
    "\n",
    "Because *state_dict* objects are Python dictionaries, they can be easily saved, updated, altered, and restored, adding a great deal of modularity to PyTorch models and optimizers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at what a `state_dict` contains on the MLP model and its associated optimizer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print the model's state dict\n",
    "print(\"MLP state_dict:\")\n",
    "for param_tensor in model.state_dict():\n",
    "  print(param_tensor, \"\\t\", model.state_dict()[param_tensor].size())\n",
    "\n",
    "\n",
    "# Print the optimizer's state dict\n",
    "print(\"Optimizer's state_dict:\")\n",
    "for var_name in optimizer.state_dict():\n",
    "  print(var_name, \"\\t\", optimizer.state_dict()[var_name])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Saving a PyTorch model's `state_dict()`\n",
    "\n",
    "The process involves calling `torch.save(obj, f)` where `obj` is the target model's `state_dict()` and `f` is the filename of where to save the model.\n",
    "\n",
    "> **Note:** It's common convention for PyTorch saved models or objects to end with `.pt` or `.pth`, like `saved_model_01.pth`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# name of the saved file\n",
    "filename = \"MLP.pth\"\n",
    "# save the model state dict on disk\n",
    "torch.save(obj=model.state_dict(),f=filename)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# check the saved model path\n",
    "!ls -l ./MLP.pth"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loading a saved PyTorch model's `state_dict()`\n",
    "\n",
    "Since we have now got a saved model `state_dict()` at `./MLP.pth` we can now load it in using `torch.nn.Module.load_state_dict(torch.load(f))` where `f` is the filepath of our saved model `state_dict()`.\n",
    "\n",
    "Why call `torch.load()` inside `torch.nn.Module.load_state_dict()`? \n",
    "\n",
    "Because we only saved the model's `state_dict()` which is a dictionary of learned parameters and not the *entire* model, we first have to load the `state_dict()` with `torch.load()` and then pass that `state_dict()` to a new instance of our model (which is a subclass of `nn.Module`).\n",
    "\n",
    "Why not save the entire model?\n",
    "\n",
    "[Saving the entire model](https://pytorch.org/tutorials/beginner/saving_loading_models.html#save-load-entire-model) rather than just the `state_dict()` is more intuitive, however, to quote the PyTorch documentation (italics mine):\n",
    "\n",
    "> The disadvantage of this approach *(saving the whole model)* is that the serialized data is bound to the specific classes and the exact directory structure used when the model is saved...\n",
    ">\n",
    "> Because of this, your code can break in various ways when used in other projects or after refactors.\n",
    "\n",
    "So instead, we are using the flexible method of saving and loading just the `state_dict()`, which again is basically a dictionary of model parameters.\n",
    "\n",
    "Let us test it out by creating another instance of `MLP()`, which is a subclass of `torch.nn.Module` and will hence have the in-built method `load_state_dict()`.\n",
    "\n",
    "> **Note:** There are more methods to save and load PyTorch models but you should check those out on your own. See the [PyTorch guide for saving and loading models](https://pytorch.org/tutorials/beginner/saving_loading_models.html#saving-and-loading-models) for more. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a new instance of our model (this will be instantiated with random weights)\n",
    "loaded_model = MLP()\n",
    "\n",
    "# Load the state_dict of our saved model (this will update the new instance of our model with the trained weights that we have saved)\n",
    "loaded_model.load_state_dict(torch.load(f=filename))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "\n",
    "2. Using the `trainer` script already provided, write a new one that also saves the *state_dict* of the model after the last epoch of training in a file that is given as parameter. \n",
    "\n",
    "3. Modify your script such that if the file already exists, the script should first load its contents into the model's *state_dict* before starting the training process.\n",
    "\n",
    "4. Modify your script so that it inspects the test set accuracy after each epoch and saves the best performing model at the end of training (i.e. the model state that gives the best test accuracy among all those at the end of an epoch)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: Write your answers here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Convolutional Neural Networks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us again use the same LeNet5 CNN architecture that we have seen in class.\n",
    "\n",
    "We first define the convolutional layers using the `nn.Conv2D` function with the appropriate kernel size and input/output channels. We also apply max pooling using the `nn.MaxPool2D` function. The nice thing about PyTorch is that we can combine convolutional layer, activation function, and max pooling into one single layer (they will be separately applied, but it helps in logically organizing the code) using the `nn.Sequential` function. We could have also combined the subsequent linear layers into one `nn.Sequential` module as well, but we wanted to show that it is possible to mix and match how things are defined inside a model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Defining the convolutional neural network\n",
    "class LeNet5(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(LeNet5, self).__init__()\n",
    "        self.conv1 = nn.Sequential(\n",
    "            nn.Conv2d(1, 6, kernel_size=5, stride=1, padding=2),\n",
    "            nn.ReLU(),\n",
    "            nn.MaxPool2d(kernel_size = 2, stride = 2))\n",
    "        self.conv2 = nn.Sequential(\n",
    "            nn.Conv2d(6, 16, kernel_size=5, stride=1, padding=0),\n",
    "            nn.ReLU(),\n",
    "            nn.MaxPool2d(kernel_size = 2, stride = 2))\n",
    "        self.fc = nn.Linear(400, 120)\n",
    "        self.relu = nn.ReLU()\n",
    "        self.fc1 = nn.Linear(120, 84)\n",
    "        self.relu1 = nn.ReLU()\n",
    "        self.fc2 = nn.Linear(84, 10)\n",
    "        \n",
    "    def forward(self, x):\n",
    "        out = self.conv1(x)\n",
    "        out = self.conv2(out)\n",
    "        out = out.reshape(out.size(0), -1)\n",
    "        out = self.fc(out)\n",
    "        out = self.relu(out)\n",
    "        out = self.fc1(out)\n",
    "        out = self.relu1(out)\n",
    "        out = self.fc2(out)\n",
    "        return out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_lenet = LeNet5().to(device=try_gpu())\n",
    "optimizer_lenet = optim.Adam(model_lenet.parameters(), lr=learning_rate)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save the train and test accuracies at the end of each epoch in two output lists from the trainer function\n",
    "train_accs_lenet, test_accs_lenet = trainer(net=model_lenet, train_loader=train_dataloader, \n",
    "                                test_loader=test_dataloader, num_epochs=num_epochs, lr=learning_rate, batch_size=batch_size, loss=cost, optimizer=optimizer_lenet)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "5. Similar to the MLP, add batch normalization layers to the convolutional layers (you need to use `nn.BatchNorm2d` this time!), train your new model and look at how it impact test accuracy. Plot again the test errors from the two models, without and with batch normalization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: write your answer here"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "base",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
