{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## MINIST Handwritten Digit Recognition in PyTorch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting up the environment\n",
    "\n",
    "Like the demo from the last lecture, we will be using PyTorch to train two models on a handwritten digit classification task using the MNIST dataset. Assuming that PyTorch is installed on your system, we just need to import the folloging libraries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The basic torch library that offers support for working with tensors, creating models, running and training them (through the use of backpropagation in the torch autodifferentiation engine)\n",
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.optim as optim\n",
    "# This library offers a set of features for working with vision models inside PyTorch (e.g. loading\n",
    "# image datasets, applying image transforms, etc.)\n",
    "import torchvision\n",
    "import torchvision.transforms as transforms\n",
    "\n",
    "# Define relevant variable for the ML task\n",
    "batch_size = 128\n",
    "num_classes = 10\n",
    "learning_rate = 0.001\n",
    "num_epochs = 10\n",
    "\n",
    "# Utility function to see if execution can be done on a CUDA-enabled GPU; if none present on your system it will default to CPU\n",
    "def try_gpu():\n",
    "    return \"cuda\" if torch.cuda.is_available() else \"cpu\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparing the Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_dataset = torchvision.datasets.MNIST('./data/', train=True, download=True,\n",
    "                                           transform=transforms.Compose([\n",
    "                                             transforms.ToTensor(),\n",
    "                                             transforms.Normalize(mean=(0.1307,), std=(0.3081,))\n",
    "                                           ])\n",
    "                             )\n",
    "\n",
    "test_dataset = torchvision.datasets.MNIST('./data/', train=True, download=True,\n",
    "                                           transform=transforms.Compose([\n",
    "                                             transforms.ToTensor(),\n",
    "                                             transforms.Normalize(mean=(0.1307,), std=(0.3081,))\n",
    "                                           ])\n",
    "                             )\n",
    "\n",
    "train_loader = torch.utils.data.DataLoader(dataset=train_dataset, \n",
    "                                           batch_size=batch_size, \n",
    "                                           shuffle=True)\n",
    "test_loader = torch.utils.data.DataLoader(dataset=test_dataset, \n",
    "                                          batch_size=batch_size, \n",
    "                                          shuffle=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a closer look at what this code does:\n",
    "- The MNIST contains images of size $28\\times 28$. We first need to convert them to tensors that can be processed with PyTorch, and next normalize them (for better numerical processing) using the pre-calculated mean and standard deviation (available online).\n",
    "- The `download=True` is set in case the data is not already downloaded on your system.\n",
    "- Next, we make use of data loaders. This is not strictly necessary, but it is good practice, since data loaders do not require having all the training data in memory at once (a real problem for huge datasets!). Data loaders allow us to iterate through the data in batches, with the data being loaded only as we iterate and not all at once at the start.\n",
    "- We specify the batch size and shuffle the dataset when loading so that every batch has some variance in the type of labels that it has. This generally improves the training process and the accuracy of the model we get at the end of training."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also examine the images we have just loaded. We'll use the test dataset through `test_loader` for this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "examples = enumerate(test_loader)\n",
    "batch_idx, (example_data, example_targets) = next(examples)\n",
    "\n",
    "print(f'Loaded data from batch {batch_idx}')\n",
    "print('Size of the batch is ', example_data.shape, ' where')\n",
    "print(f'The first value is the number of samples in the batch: {example_data.shape[0]}')\n",
    "print(f'The second value is the number of input channel of the input: {example_data.shape[1]}')\n",
    "print(f'And the last two values are the shape of an image channel: {example_data.shape[2]} x {example_data.shape[3]}')\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize the data, we can use `matplotlib`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fig = plt.figure()\n",
    "for i in range(6):\n",
    "  plt.subplot(2, 3, i+1)\n",
    "  plt.tight_layout()\n",
    "  plt.imshow(example_data[i][0], cmap='gray', interpolation='none')\n",
    "  plt.title(\"Ground Truth: {}\".format(example_targets[i]))\n",
    "  plt.xticks([])\n",
    "  plt.yticks([])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A multilayer perceptron for MNIST\n",
    "\n",
    "We are now ready to build our first network, a three layer MLP. If you remember, a liner layer takes 1D-inputs, so we need to transform our 2D images into 1D vectors. We will do this with the `torch.view` method that allows us to change the shape of a tensor without changing its contents (we will look at how it works in more detail in the first Practical Session (TP) for this course). Since we are looking at a classification problem, we are going to use a cross-entropy loss, which in `torch` is available through `torch.nn.CrossEntropy`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In PyTorch, you construct new models by transforming an input through various layers, which are always subclasses of [`torch.nn.Module`](https://pytorch.org/docs/stable/nn.html#torch.nn.Module). Since it is good practice, we will define our own class when building new models by subclassing `torch.nn.Module`, which provides already a bunch of useful methods. This subclassing of the `Module` class usually consists only in redefining the constructor (the `__init__` method) and the [forward method](https://pytorch.org/docs/stable/generated/torch.nn.Module.html#torch.nn.Module.forward)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MLP(nn.Module):\n",
    "  def __init__(self):\n",
    "    super(MLP, self).__init__()\n",
    "    self.l1 = nn.Linear(28 * 28, 128)\n",
    "    self.relu1 = nn.ReLU()\n",
    "    self.l2 = nn.Linear(128, 96)\n",
    "    self.relu2 = nn.ReLU()\n",
    "    self.l3 = nn.Linear(96, 10)\n",
    "\n",
    "  def forward(self, x):\n",
    "    x = x.reshape(-1, 28 * 28)\n",
    "    out = self.relu1(self.l1(x))\n",
    "    out = self.relu2(self.l2(out))\n",
    "    out = self.l3(out)\n",
    "    return out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting the hyperparameters\n",
    "\n",
    "Before training our new model, we need to set some hyperparameters, such as the optimizer to use (SGD in this case) and the cross-entropy loss function we mentioned before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instantiating the model and moving it to the right device (GPU if available on the system)\n",
    "model = MLP().to(device=try_gpu())\n",
    "\n",
    "# Setting the loss function\n",
    "cost = nn.CrossEntropyLoss()\n",
    "\n",
    "# Setting the optimizer with the model parameters and learning rate\n",
    "optimizer = optim.SGD(model.parameters(), lr=learning_rate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training and testing the network\n",
    "\n",
    "Training our model means iterating over the mini batches of the training set and, for each mini-batch, computing the forward and backward passes that are needed to provide the optimizer with the gradient it requires to update the model paremeters. Below is a prototypical function for a training function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tqdm import tqdm\n",
    "\n",
    "# Utility function to evaluate the accuracy\n",
    "def get_performance_metrics(net, iter, loss_fn, device=try_gpu()):\n",
    "    # We enter evaluation mode. This is useless for the linear model\n",
    "    # but is important with layers such as dropout, batchnorm, ..\n",
    "    net.eval()\n",
    "    tot_loss, num_correct, num_total = 0.0, 0.0, 0.0\n",
    "    # We disable gradient computation which speeds up the overall computation \n",
    "    # and reduces memory usage \n",
    "    with torch.no_grad():\n",
    "        for i, (X, y) in enumerate(iter):\n",
    "            # We got a minibatch from the loader within inputs and targets\n",
    "            # We need to copy the data on the GPU if we use one\n",
    "            X, y = X.to(device), y.to(device)\n",
    "            # Compute the forward pass, i.e., the scores for each input image\n",
    "            yhat = net(X)\n",
    "\n",
    "            # We accumulate the loss considering\n",
    "            # The multipliation by inputs.shape[0] is due to the fact\n",
    "            # that our loss criterion is averaging over its samples\n",
    "            tot_loss += X.shape[0] * loss_fn(yhat, y).item()\n",
    "\n",
    "            # For the accuracy, we compute the labels for each input image\n",
    "            # Be carefull, the model is outputing scores and not the probabilities\n",
    "            # But given the softmax is not altering the rank of its input scores\n",
    "            # we can compute the label by argmaxing directly the scores\n",
    "            predicted = yhat.argmax(dim=1)\n",
    "            correct = (predicted == y).sum()\n",
    "            num_correct += correct.item()\n",
    "            # We accumulate the exact number of processed samples\n",
    "            num_total += y.shape[0]\n",
    "    return tot_loss / num_total, num_correct / num_total\n",
    "\n",
    "\n",
    "def get_lr(optimizer):\n",
    "    for param_group in optimizer.param_groups:\n",
    "        return param_group[\"lr\"]\n",
    "\n",
    "def trainer(\n",
    "    net,\n",
    "    train_loader,\n",
    "    test_loader,\n",
    "    num_epochs,\n",
    "    lr,\n",
    "    batch_size,\n",
    "    loss,\n",
    "    optimizer,\n",
    "    device=try_gpu()\n",
    "):\n",
    "    # 1. Move the model to the appropriate device for training\n",
    "    net.to(device)\n",
    "    train_acc_list = []\n",
    "    test_acc_list = []\n",
    "\n",
    "    # 2. the training loop\n",
    "    for epoch in range(num_epochs):\n",
    "        # use a tqdm progress bar to see how training progresses\n",
    "        tq = tqdm(total=len(train_loader) * batch_size)\n",
    "        tq.set_description(f\"Epoch {epoch}, lr {get_lr(optimizer):.3f}\")\n",
    "        net.train()\n",
    "        train_acc, train_loss, train_size = 0.0, 0.0, 0.0\n",
    "        for X, y in train_loader:\n",
    "            X, y = X.to(device), y.to(device)\n",
    "            optimizer.zero_grad()\n",
    "            # compute the output\n",
    "            yhat = net(X)\n",
    "            # compute loss and perform back-propagation\n",
    "            l = loss(yhat, y)\n",
    "\n",
    "            # scale gradient and record loss\n",
    "            l.backward()\n",
    "            optimizer.step()\n",
    "\n",
    "            tq.update(batch_size)\n",
    "            with torch.no_grad():\n",
    "                train_loss += l.data.item() * X.shape[0]\n",
    "                pred = yhat.argmax(dim=1)\n",
    "            train_acc += (pred == y).sum().item()\n",
    "            train_size += X.shape[0]\n",
    "\n",
    "            tq.set_postfix(\n",
    "                train_acc=\"{:.5f}\".format(train_acc / train_size),\n",
    "                train_loss=\"{:.5f}\".format(train_loss / train_size),\n",
    "            )\n",
    "        tq.close()\n",
    "\n",
    "        _, test_acc = get_performance_metrics(net, test_loader, loss, device)\n",
    "        train_acc_list.append(train_acc / train_size)\n",
    "        test_acc_list.append(test_acc)\n",
    "\n",
    "        print(\n",
    "            f\"train loss estimate {train_loss / train_size:.3f}, train acc {train_acc / train_size:.3f}, test acc {test_acc:.3f}\"\n",
    "        )\n",
    "\n",
    "    return train_acc_list, test_acc_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to train the network and examine its loss and accuracy on the test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_accs_mlp, test_accs_mlp = trainer(net=model, train_loader=train_loader, \n",
    "                                test_loader=test_loader, num_epochs=num_epochs, lr=learning_rate, batch_size=batch_size, loss=cost, optimizer=optimizer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can plot the accuracy of the model in both the training and test scenarios to see how they evolve over the course of the training procedure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(range(num_epochs), [acc * 100 for acc in train_accs_mlp], label=\"Training Accuracy\")\n",
    "plt.plot(range(num_epochs), [acc * 100 for acc in test_accs_mlp], label=\"Test Accuracy\")\n",
    "plt.xlabel(\"Epoch\")\n",
    "plt.ylabel(\"Accuracy\")\n",
    "plt.legend()\n",
    "plt.title(\"MLP\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### LeNet5 on MNIST\n",
    "\n",
    "Writing the code for a LeNet5 CNN is equally straightforward."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Defining the convolutional neural network\n",
    "class LeNet5(nn.Module):\n",
    "    def __init__(self, num_classes):\n",
    "        super(LeNet5, self).__init__()\n",
    "        self.layer1 = nn.Sequential(\n",
    "            nn.Conv2d(1, 6, kernel_size=5, stride=1, padding=2),\n",
    "            nn.BatchNorm2d(6),\n",
    "            nn.ReLU(),\n",
    "            nn.MaxPool2d(kernel_size = 2, stride = 2))\n",
    "        self.layer2 = nn.Sequential(\n",
    "            nn.Conv2d(6, 16, kernel_size=5, stride=1, padding=0),\n",
    "            nn.BatchNorm2d(16),\n",
    "            nn.ReLU(),\n",
    "            nn.MaxPool2d(kernel_size = 2, stride = 2))\n",
    "        self.fc = nn.Linear(400, 120)\n",
    "        self.relu = nn.ReLU()\n",
    "        self.fc1 = nn.Linear(120, 84)\n",
    "        self.relu1 = nn.ReLU()\n",
    "        self.fc2 = nn.Linear(84, num_classes)\n",
    "        \n",
    "    def forward(self, x):\n",
    "        out = self.layer1(x)\n",
    "        out = self.layer2(out)\n",
    "        out = out.reshape(out.size(0), -1)\n",
    "        out = self.fc(out)\n",
    "        out = self.relu(out)\n",
    "        out = self.fc1(out)\n",
    "        out = self.relu1(out)\n",
    "        out = self.fc2(out)\n",
    "        return out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the architecture itself, we first define the convolutional layers using the `nn.Conv2D` function with the appropriate kernel size and input/output channels. We also apply max pooling using the `nn.MaxPool2D` function. The nice thing about PyTorch is that we can combine convolutional layer, activation function, and max pooling into one single layer (they will be separately applied, but it helps in logically organizing the code) using the `nn.Sequential` function. We could have also combined the subsequent linear layers into one `nn.Sequential` module as well, but we wanted to show that it is possible to mix and match how things are defined inside a model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the optimizer, we will change it for this second example and use `optim.Adam`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = LeNet5(10).to(device=try_gpu())\n",
    "optimizer = optim.Adam(model.parameters(), lr=learning_rate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now again ready to run the training loop on the LeNet CNN. You can see that the final test accuracy is much better than that of our initial MLP."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_accs_lenet, test_accs_lenet = trainer(net=model, train_loader=train_loader, \n",
    "                                test_loader=test_loader, num_epochs=num_epochs, lr=learning_rate, batch_size=batch_idx, loss=cost, optimizer=optimizer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, we can examine how the accuracy evolves over the course of 10 training epochs and compare the result with the one for the previous MLP network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplot(1, 2, 1)\n",
    "plt.plot(range(num_epochs), [acc * 100 for acc in train_accs_mlp], label=\"Training Accuracy\")\n",
    "plt.plot(range(num_epochs), [acc * 100 for acc in test_accs_mlp], label=\"Test Accuracy\")\n",
    "plt.xlabel(\"Epoch\")\n",
    "plt.ylabel(\"Accuracy\")\n",
    "plt.legend()\n",
    "plt.title(\"MLP\")\n",
    "\n",
    "plt.subplot(1, 2, 2)\n",
    "plt.plot(range(num_epochs), [acc * 100 for acc in train_accs_lenet], label=\"Training Accuracy\")\n",
    "plt.plot(range(num_epochs), [acc * 100 for acc in test_accs_lenet], label=\"Test Accuracy\")\n",
    "plt.xlabel(\"Epoch\")\n",
    "plt.ylabel(\"Accuracy\")\n",
    "plt.legend()\n",
    "plt.title(\"LeNet5\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "base",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
