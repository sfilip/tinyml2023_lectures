{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A 1D Regression Example using PyTorch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import required libraries (torch for working with tensor data)\n",
    "import torch\n",
    "import torch.nn as nn\n",
    "# Matplotlib + numpy for plotting\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up a training dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The Training Dataset\n",
    "X = torch.tensor([0.03, 0.19, 0.34, 0.46, 0.78, 0.81, 1.08, 1.18, 1.39, 1.60, 1.65, 1.90])\n",
    "y = torch.tensor([0.67, 0.85, 1.05, 1.0, 1.40, 1.5, 1.3, 1.54, 1.55, 1.68, 1.73, 1.6])\n",
    "\n",
    "# Prepare the dataset for training\n",
    "from torch.utils.data import TensorDataset, DataLoader\n",
    "def load_array(data_arrays, batch_size):\n",
    "  dataset = TensorDataset(*data_arrays)\n",
    "  dataloader = DataLoader(dataset=dataset, batch_size=batch_size, shuffle=True)\n",
    "  return dataloader\n",
    "\n",
    "# Load the dataset\n",
    "dataloader = load_array((X, y), batch_size=len(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constructing the linear regression model\n",
    "\n",
    "We start off with specializing a `torch.nn.Module` class, in order to create the model.\n",
    "$$\n",
    "m(x)=w\\cdot x + b.\n",
    "$$\n",
    "The fact that $w$ and $b$ are trainable parameters is specified by initializing them as `nn.Parameter` objects. This will let PyTorch know that it will need to compute gradients for them using its automatic differentiation engine (more on this during the TPs).\n",
    "\n",
    "PyTorch also comes with many predefined loss functions. In our case, `nn.MSELoss`, is the mean square error loss we are looking for. For a **training** dataset \n",
    "$$\n",
    "  \\mathcal{D}=\\{ \\left(x_i,y_i\\right) \\}_{i=1}^n,\n",
    "$$\n",
    "it will compute the loss function\n",
    "$$\n",
    "\\mathcal{L}(w,b) = \\frac{1}{n}\\sum_{i=1}^n\\left(m(x_i)-y_i\\right)^2.\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The linear regression model\n",
    "class LinearRegression(nn.Module):\n",
    "  def __init__(self):\n",
    "    super(LinearRegression, self).__init__()\n",
    "    self.w = nn.Parameter(torch.tensor(0.0))\n",
    "    self.b = nn.Parameter(torch.tensor(0.0))\n",
    "\n",
    "  def forward(self, x):\n",
    "    return self.b + self.w * x\n",
    "  \n",
    "# Instantiate a model  \n",
    "m = LinearRegression()\n",
    "\n",
    "# Loss function for the model   \n",
    "loss = nn.MSELoss(reduction='sum')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Utility function for plotting the dataset and the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot(x, y, model):\n",
    "    fig,ax = plt.subplots()\n",
    "    x, y = x.detach().numpy(), y.detach().numpy()\n",
    "    ax.scatter(x,y)\n",
    "    plt.xlim([0,2.0])\n",
    "    plt.ylim([0,2.0])\n",
    "    ax.set_xlabel('Output')\n",
    "    ax.set_ylabel('Input')\n",
    "    # Draw line\n",
    "    x_line = np.arange(0,2,0.01)\n",
    "    with torch.no_grad():\n",
    "        y_line = model(torch.from_numpy(x_line).view(-1, 1).float())\n",
    "    plt.plot(x_line, y_line.detach().numpy(),'b-',lw=2)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How gradient descent is performed using PyTorch\n",
    "\n",
    "We create a `torch.optim.SGD` optimizer object that will perform iterations of the gradient descent algorithm. We use a fixed learning rate parameter $\\alpha=0.01$. In the `train()` function, the gradients of the loss function $\\partial\\mathcal{L}/\\partial w$ and $\\partial\\mathcal{L}/\\partial b$ by making a call to the `backward()` method on the `loss` object. An iteration (or **step**) of the update rules\n",
    "$$\n",
    "w=w-\\alpha\\frac{\\partial \\mathcal{L}}{\\partial w}\n",
    "$$\n",
    "and\n",
    "$$\n",
    "b=b-\\alpha\\frac{\\partial \\mathcal{L}}{\\partial b}\n",
    "$$\n",
    "is performed with the call to `optimizer.step()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the optimizer to be (stochastic) gradient descent\n",
    "optimizer = torch.optim.SGD(m.parameters(), lr=1e-2)\n",
    "\n",
    "def train(\n",
    "  model: nn.Module,\n",
    "  dataflow: DataLoader,\n",
    "  criterion: nn.Module,\n",
    "  optimizer: torch.optim.Optimizer,\n",
    ") -> None:\n",
    "  model.train()\n",
    "\n",
    "  for inputs, targets in dataflow:\n",
    "\n",
    "    # Reset the gradients (from the last iteration)\n",
    "    optimizer.zero_grad()\n",
    "\n",
    "    # Forward inference\n",
    "    outputs = model(inputs)\n",
    "    loss = criterion(outputs, targets)\n",
    "\n",
    "    # Backward propagation\n",
    "    loss.backward()\n",
    "\n",
    "    # Update optimizer\n",
    "    optimizer.step()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialize the model and plot it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.w.data, m.b.data = torch.tensor(0.0), torch.tensor(1.0)\n",
    "plot(X, y, m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Perform an iteration of gradient descent"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train(m, dataloader, loss, optimizer)\n",
    "with torch.no_grad():\n",
    "    plot(X, y, m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Perform $99$ more iterations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(99):\n",
    "  train(m, dataloader, loss, optimizer)\n",
    "\n",
    "with torch.no_grad():\n",
    "    plot(X, y, m)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "base",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
